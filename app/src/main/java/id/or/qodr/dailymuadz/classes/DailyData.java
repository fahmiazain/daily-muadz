package id.or.qodr.dailymuadz.classes;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import id.or.qodr.dailymuadz.DetailNoteStudent;
import id.or.qodr.dailymuadz.R;
import id.or.qodr.dailymuadz.adapters.ArrayAdapterFactory;
import id.or.qodr.dailymuadz.adapters.DailyAdapter;
import id.or.qodr.dailymuadz.adapters.StudentAdapter;
import id.or.qodr.dailymuadz.adapters.StudentNoteAdapter;
import id.or.qodr.dailymuadz.model.DailyModel;
import id.or.qodr.dailymuadz.model.LessionModel;
import id.or.qodr.dailymuadz.model.StudentModel;
import id.or.qodr.dailymuadz.model.StudentNoteModel;


/**
 * Created by muhammad on 18/11/16.
 */

public class DailyData {

    private static final String LOG = "DailyData";
    private ArrayList<DailyModel> daily = null;
    private ArrayList<LessionModel> lession = null;
    private ArrayList<StudentModel> student = null;
    private ArrayList<StudentNoteModel> student_note = null;
    DailyAdapter dailyAdapter;
    private static final String URL_LIST_USER_STUDENT = "http://192.168.1.6:1212/api/student/";
    private final String END_URL="/notes";
    private String URL_LIST_ACTIVITY="http://192.168.1.6:1212/api/users/1/daily";

    private Context context;
    private ProgressDialog progress;
    private DateFormat currentDate;
    private Date date;
    private StaggeredGridLayoutManager layoutManager;
    private LinearLayoutManager linearLayoutManager;
    private MaterialDialog dialog;
    private MaterialDialog studentDialog;
    private Button lession_name;
    private View view;
    private MaterialDialog lession_text;

    public DailyData(Context context) {
        this.context = context;
    }

    public void getDaily(final RecyclerView recyclerDaily, String URL) {
        JsonObjectRequest request = new JsonObjectRequest(URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    Gson gson = new GsonBuilder().registerTypeAdapterFactory(new ArrayAdapterFactory()).create();
                    DailyParser result = gson.fromJson(response.toString(), DailyParser.class);

                    daily = result.getUser().getDaily();
                    layoutManager = new StaggeredGridLayoutManager(2, GridLayoutManager.VERTICAL);

                    recyclerDaily.setHasFixedSize(true);
                    recyclerDaily.setLayoutManager(layoutManager);

                    dailyAdapter = new DailyAdapter(context, daily);

                    recyclerDaily.setAdapter(dailyAdapter);
                    dailyAdapter.notifyDataSetChanged();

                } catch (Exception e) {
                    Log.d("Exception", e.toString());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                Log.d(LOG, error.toString());
            }
        }
        );

        RequestQueue queue = Volley.newRequestQueue(context);

        request.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

    public void getStudent(final RecyclerView recyclerStudent, String URL) {
        JsonObjectRequest request = new JsonObjectRequest(URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Gson gson = new GsonBuilder().registerTypeAdapterFactory(new ArrayAdapterFactory()).create();
                    ParsingResult result = gson.fromJson(response.toString(), ParsingResult.class);

                    student = result.getStudent();
                    linearLayoutManager = new LinearLayoutManager(context);

                    recyclerStudent.setHasFixedSize(true);
                    recyclerStudent.setLayoutManager(linearLayoutManager);

                    final StudentAdapter rcAdapter = new StudentAdapter(context, student);
                    recyclerStudent.setAdapter(rcAdapter);

                } catch (Exception e) {
                    Log.d("Exception", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                Log.d(LOG, error.toString());
            }
        }
        );

        RequestQueue queue = Volley.newRequestQueue(context);

        request.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

    public void searchStudent(String url, final RecyclerView rcvView, final String student_name ) {
        StringRequest search = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Gson gson = new GsonBuilder().registerTypeAdapterFactory(new ArrayAdapterFactory()).create();
                    ParsingResult result = gson.fromJson(response.toString(), ParsingResult.class);

                    student = result.getStudent();
                    linearLayoutManager = new LinearLayoutManager(context);

                    rcvView.setHasFixedSize(true);
                    rcvView.setLayoutManager(linearLayoutManager);

                    final StudentAdapter rcAdapter = new StudentAdapter(context, student);
                    rcvView.setAdapter(rcAdapter);

                } catch (Exception e) {
                    Log.d("Exception", e.toString());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "ErrorsearchStudent", Toast.LENGTH_SHORT).show();
                Log.d("error ", error.toString());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", student_name);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(search);
    }

    public void getStudentNote(final RecyclerView rcvNoteStudent, String URL) {
        JsonObjectRequest request = new JsonObjectRequest(URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Gson gson = new GsonBuilder().registerTypeAdapterFactory(new ArrayAdapterFactory()).create();
                    DailyParser result = gson.fromJson(response.toString(), DailyParser.class);

                    student_note = result.getStudent().getStudentNote();
                    layoutManager = new StaggeredGridLayoutManager(2, GridLayoutManager.VERTICAL);

                    rcvNoteStudent.setHasFixedSize(true);
                    rcvNoteStudent.setLayoutManager(layoutManager);

                    final StudentNoteAdapter rcAdapter = new StudentNoteAdapter(context.getApplicationContext(), student_note);
                    rcvNoteStudent.setAdapter(rcAdapter);
                    rcAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    Log.d("Exception: ", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                Log.d(LOG, error.toString());
            }
        }
        );

        RequestQueue queue = Volley.newRequestQueue(context);

        request.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);

    }
    public void editDaily(String url, final RecyclerView recyclerVw, final MaterialDialog dialog, final String lession_id, final String time_lession, final String clas_at) {

        final TextView section_less = (TextView) dialog.findViewById(R.id.section_lession);
        StringRequest edit = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    getDaily(recyclerVw, URL_LIST_ACTIVITY);

                    recyclerVw.setAdapter(dailyAdapter);
                    dailyAdapter.notifyDataSetChanged();
                    progress.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Gagal Edit, Coba Lagi!", Toast.LENGTH_SHORT).show();
                Log.d("error", error.toString());
//                progress.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();

                map.put("class_at", clas_at);
                map.put("lession_id", lession_id);
                map.put("section_lession", section_less.getText().toString());
                map.put("time_lession", time_lession);
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(edit);
    }

    public void editNoteStudent(String url, MaterialDialog dialog, final String category) {
        final TextView note_student = (TextView) dialog.findViewById(R.id.student_note);
        StringRequest edit = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    Toast.makeText(context, "editNoteSukses", Toast.LENGTH_SHORT).show();
                    progress.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Gagal Edit, Coba Lagi!", Toast.LENGTH_SHORT).show();
                Log.d("error", error.toString());
//
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("category", category);
                map.put("student_note", note_student.getText().toString());
                return map;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(edit);
    }

    public void addDailyActivity(final Context context, final String url, final RecyclerView recyclerVw, final String autourl) {


        boolean wrapInScrollView = true;
        dialog = new MaterialDialog.Builder(context)
                .title(R.string.string_add_activity)
                .customView(R.layout.view_form_daily, wrapInScrollView)
                .positiveText(android.R.string.ok)
                .negativeText(android.R.string.cancel)
                .cancelable(false)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                        EditText section_lession = (EditText) dialog.getView().findViewById(R.id.section_lession);

                        Spinner class_at =  (Spinner) dialog.getCustomView().findViewById(R.id.class_at);

                        Spinner time_lession = (Spinner) dialog.getCustomView().findViewById(R.id.time_lession);

                        TextView lession_id = (TextView) dialog.getCustomView().findViewById(R.id.lession_id);

                            if (dailyValidation(section_lession, "Harus di isi!")){

                                currentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                date = new Date();

                                progress = new ProgressDialog(context);
                                progress.setMessage("Mohon Tunggu...");
                                progress.show();

                                HashMap<String, String> params = new HashMap<String, String>();
                                params.put("users_id", "1");
                                params.put("class_at", class_at.getSelectedItem().toString());
                                params.put("lession_id", lession_id.getText().toString());
                                params.put("section_lession", section_lession.getText().toString());
                                params.put("time_lession", time_lession.getSelectedItem().toString());
                                params.put("created_by", "admin");
                                params.put("updated_by", "admin");
                                params.put("created_at", currentDate.format(date));
                                params.put("updated_at", currentDate.format(date));
                                submitDaily(url, params, dialog, recyclerVw, autourl, "");
                            }

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        dialog.dismiss();
                    }
                })
                .build();

        final Spinner class_at = (Spinner) dialog.getCustomView().findViewById(R.id.class_at);
        Spinner time_lession = (Spinner) dialog.getCustomView().findViewById(R.id.time_lession);

        class_at.setPrompt("Pilih Kelas");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, CLASS_ID);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        class_at.setAdapter(dataAdapter);


        ArrayAdapter<String> timeLessionAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, TIME_ID);
        timeLessionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        time_lession.setAdapter(timeLessionAdapter);

        lession_name = (Button) dialog.getCustomView().findViewById(R.id.lession_name);
        final TextView lession_id = (TextView) dialog.getCustomView().findViewById(R.id.lession_id);
        lession_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                JsonObjectRequest request = new JsonObjectRequest("http://192.168.1.6:1212/api/lessions", null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Gson gson = new GsonBuilder().registerTypeAdapterFactory(new ArrayAdapterFactory()).create();
                        ParsingResult result = gson.fromJson(response.toString(), ParsingResult.class);
                        lession = result.getLession();

                        final ArrayList<String> listLessionName = new ArrayList<String>();
                        if (lession != null) {
                            for (int i = 0; i < lession.size(); i++) {
                                listLessionName.add(lession.get(i).lession_name.toString());
                            }
                        }

                        final ArrayList<Integer> listLessionID = new ArrayList<Integer>();
                        if (lession != null) {
                            for (int i = 0; i < lession.size(); i++) {
                                listLessionID.add(Integer.parseInt(lession.get(i).id));
                            }
                        }

                        lession_text = new MaterialDialog.Builder(context)
                                .title(R.string.lession_name)
                                .items(listLessionName)
                                .cancelable(false)
                                .itemsCallback(new MaterialDialog.ListCallback() {
                                    @Override
                                    public void onSelection(MaterialDialog dialog, View view, int posision, CharSequence text) {
                                        lession_name.setText(text);
                                        lession_id.setText(listLessionID.get(posision).toString());
//                                        Toast.makeText(context, "" + text,Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .positiveText(android.R.string.cancel)
                                .show();

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                        Log.d(LOG, error.toString());
                    }
                }
                );

                RequestQueue queue = Volley.newRequestQueue(context);
                queue.add(request);

            }
        });

        dialog.show();
    }

    public void addNoteStudent(final Context context, final String url, final RecyclerView recyclerView, final String autoUrl, final String idRequest, final String getName) {
        boolean wrapInScrollView = true;
        studentDialog = new MaterialDialog.Builder(context)
                .title("Tambah catatan " + getName)
                .customView(R.layout.view_form_note, wrapInScrollView)
                .positiveText(android.R.string.ok)
                .negativeText(android.R.string.cancel)
                .cancelable(false)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        EditText add_note_student = (EditText) dialog.getView().findViewById(R.id.student_note);

                        final Spinner id_category = (Spinner) dialog.getCustomView().findViewById(R.id.category);


                        if (noteStudentValidation(add_note_student, "Harus di isi!")) {

                            currentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            date = new Date();

                            progress = new ProgressDialog(context);
                            progress.setMessage("Mohon Tunggu...");
                            progress.show();

                            HashMap<String, String> params = new HashMap<String, String>();
                            params.put("student_id", idRequest);
                            params.put("student_note", add_note_student.getText().toString());
                            params.put("category", id_category.getSelectedItem().toString());
                            params.put("users_id", "1");
                            params.put("created_by", "admin");
                            params.put("updated_by", "admin");
                            params.put("created_at", currentDate.format(date));
                            params.put("updated_at", currentDate.format(date));
                            submitDaily(url, params, dialog, recyclerView, autoUrl, idRequest);
                        }
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        dialog.dismiss();
                    }
                })
                .build();
        final Spinner id_category = (Spinner) studentDialog.getCustomView().findViewById(R.id.category);

        ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, CATEGORY_ID);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        id_category.setAdapter(categoryAdapter);

        studentDialog.show();
    }

    private void submitDaily(String url, final HashMap<String, String> params, final MaterialDialog dialog, final RecyclerView recyclerVw, final String autourl, final String idRequest) {

        StringRequest post = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    dialog.dismiss();
                    Toast.makeText(context, "Berhasil Menginput", Toast.LENGTH_SHORT).show();
                    progress.dismiss();

                    if (!TextUtils.isEmpty(idRequest) ) {
                        getStudent(recyclerVw, URL_LIST_USER_STUDENT+idRequest+END_URL);
                        Intent intent = new Intent(context, DetailNoteStudent.class);
                        intent.putExtra("idStudent", idRequest);
                        context.startActivity(intent);
                    }else{
                        getDaily(recyclerVw, URL_LIST_ACTIVITY);

                        recyclerVw.setAdapter(dailyAdapter);
                    }
                    Log.d("response", response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Gagal Input, Coba Lagi!", Toast.LENGTH_SHORT).show();
                progress.dismiss();
                Log.d("error", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = params;
                return map;
            }

        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(post);
    }

    private boolean noteStudentValidation(EditText class_at, String error_messages) {
        if (TextUtils.isEmpty(class_at.getText().toString()) || studentDialog.getSelectedIndex()!=-1) {
            class_at.setError(error_messages);
            return false;
        }
        return true;

    }

    private boolean dailyValidation(EditText class_at, String error_messages) {
        if (TextUtils.isEmpty(class_at.getText().toString()) || lession_name.isSelected() ==true) {
            class_at.setError(error_messages);
            lession_name.setError(error_messages);
            return false;
        }
        return true;
    }

    private static final String[] CATEGORY_ID = new String[] {
            "Prestasi","Peringatan","Hukuman","Apresiasi","Umum"
    };

    private static final String[] CLASS_ID = new String[]{
            "1", "2", "3", "4", "5", "6"
    };

    private static final String[] TIME_ID = new String[]{
            "1", "2", "3", "4", "5"
    };


}
