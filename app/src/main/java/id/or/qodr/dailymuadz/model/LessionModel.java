package id.or.qodr.dailymuadz.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by muhammad on 27/11/16.
 */
public class LessionModel implements Parcelable{
    public String id, lession_name, created_at, updated_at;

    protected LessionModel(Parcel in) {
        id = in.readString();
        lession_name = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
    }

    public static final Creator<LessionModel> CREATOR = new Creator<LessionModel>() {
        @Override
        public LessionModel createFromParcel(Parcel in) {
            return new LessionModel(in);
        }

        @Override
        public LessionModel[] newArray(int size) {
            return new LessionModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(lession_name);
        dest.writeString(created_at);
        dest.writeString(updated_at);
    }
}
