package id.or.qodr.dailymuadz.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import id.or.qodr.dailymuadz.R;

/**
 * Created by muhammad on 18/11/16.
 */
public class DailyVHolder extends RecyclerView.ViewHolder {

    public TextView class_at, lession_id, section_lession, time_lession,daily_id;

    public DailyVHolder(View itemView) {
        super(itemView);
        daily_id = (TextView) itemView.findViewById(R.id.daily_id);
        class_at = (TextView) itemView.findViewById(R.id.class_at);
        lession_id = (TextView) itemView.findViewById(R.id.lession_id);
        section_lession = (TextView) itemView.findViewById(R.id.section_lession);
        time_lession = (TextView) itemView.findViewById(R.id.time_lession);
    }

}