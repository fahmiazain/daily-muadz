package id.or.qodr.dailymuadz.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import id.or.qodr.dailymuadz.DetailNoteStudent;
import id.or.qodr.dailymuadz.R;
import id.or.qodr.dailymuadz.classes.DailyData;
import id.or.qodr.dailymuadz.model.StudentModel;

/**
 * Created by adul on 29/11/16.
 */

public class StudentAdapter extends RecyclerView.Adapter<StudentVHolder> {

    private static final String URL_ADD_NOTE_STUDENT = "http://192.168.1.6:1212/api/notes/add";
    private static final String URL_LIST_NOTE_STUDENT = "http://192.168.1.6:1212/api/student";
    private RecyclerView rvNote;
    private List<StudentModel> studentList;
    private DailyData dailyData;
    private Context context;

    public StudentAdapter(Context context, List<StudentModel> studentList) {
        this.context = context;
        this.studentList = studentList;
    }

    @Override
    public StudentVHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_list_student,null);
        StudentVHolder rcv = new StudentVHolder(layoutView);

        return rcv;
    }

    @Override
    public void onBindViewHolder(StudentVHolder holder, final int position) {
        holder.NIS.setText("NIS : " + studentList.get(position).NIS);
        holder.student_name.setText("Nama : " + studentList.get(position).student_name);
        holder.class_student.setText("Kelas : " + studentList.get(position).class_at);
        holder.notes_count.setText(studentList.get(position).notes_count+" Komentar");
        final String getID=String.valueOf(studentList.get(position).id);
        final String getName=String.valueOf(studentList.get(position).student_name);
        holder.add_note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                studentList = new ArrayList<StudentModel>();
                dailyData = new DailyData(view.getContext());

                rvNote = (RecyclerView) view.findViewById(R.id.recyclerViewDaily);
                dailyData.addNoteStudent(view.getContext(), URL_ADD_NOTE_STUDENT, rvNote, URL_LIST_NOTE_STUDENT, getID,getName);

            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                studentList = new ArrayList<StudentModel>();
                dailyData = new DailyData(view.getContext());
                rvNote = (RecyclerView) view.findViewById(R.id.recyclerViewDaily);
                Intent intent = new Intent(context, DetailNoteStudent.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("idStudent", getID);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.studentList.size();
    }
}
