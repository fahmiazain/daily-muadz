package id.or.qodr.dailymuadz.classes;

import java.util.ArrayList;

import id.or.qodr.dailymuadz.model.DailyModel;
import id.or.qodr.dailymuadz.model.LessionModel;
import id.or.qodr.dailymuadz.model.StudentModel;
import id.or.qodr.dailymuadz.model.StudentNoteModel;
import id.or.qodr.dailymuadz.model.UsersModel;

/**
 * Created by muhammad on 18/11/16.
 */

public class DailyParser {

    public ParsingResult user;

    public ParsingResult student;

    public ParsingResult getUser() {
        return user;
    }

    public ParsingResult getStudent() {
        return student;
    }
}

class ParsingResult {

    public ArrayList<DailyModel> daily;

    public ArrayList<DailyModel> getDaily() {
        return daily;
    }

    public ArrayList<LessionModel> lession;

    public ArrayList<LessionModel> getLession() {
        return lession;
    }

    public ArrayList<StudentModel> student;

    public ArrayList<StudentModel> getStudent() {
        return student;
    }

    public ArrayList<StudentNoteModel> student_note;

    public ArrayList<StudentNoteModel> getStudentNote() {
        return student_note;
    }


}
