package id.or.qodr.dailymuadz.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import id.or.qodr.dailymuadz.R;

/**
 * Created by adul on 29/11/16.
 */
public class StudentVHolder extends RecyclerView.ViewHolder {

    public TextView NIS, student_name, class_student, notes_count;
    Button add_note;

    public StudentVHolder(View itemView) {
        super(itemView);
        NIS = (TextView) itemView.findViewById(R.id.nis);
        student_name = (TextView) itemView.findViewById(R.id.student_name);
        class_student = (TextView) itemView.findViewById(R.id.class_student);
        notes_count = (TextView) itemView.findViewById(R.id.count);
        add_note = (Button) itemView.findViewById(R.id.btn_note_student);
    }
}
