package id.or.qodr.dailymuadz.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by adul on 06/12/16.
 */

public class UsersModel implements Parcelable {

    public String id, username, full_name, email, status,
            level_id, created_by, updated_by, created_at, updated_at, last_accessed;

    protected UsersModel(Parcel in) {
        id = in.readString();
        username = in.readString();
        full_name = in.readString();
        email = in.readString();
        status= in.readString();
        level_id= in.readString();
        created_by = in.readString();
        updated_by = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
        last_accessed = in.readString();
    }

    public static final Creator<UsersModel> CREATOR = new Creator<UsersModel>() {
        @Override
        public UsersModel createFromParcel(Parcel in) {
            return new UsersModel(in);
        }

        @Override
        public UsersModel[] newArray(int size) {
            return new UsersModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        dest.writeString(id);
        dest.writeString(username);
        dest.writeString(full_name);
        dest.writeString(email);
        dest.writeString(status);
        dest.writeString(level_id);
        dest.writeString(created_by);
        dest.writeString(updated_by);
        dest.writeString(created_at);
        dest.writeString(updated_at);
        dest.writeString(last_accessed);
    }
}
