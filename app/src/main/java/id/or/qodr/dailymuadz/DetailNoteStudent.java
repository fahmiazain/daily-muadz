package id.or.qodr.dailymuadz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.or.qodr.dailymuadz.adapters.ArrayAdapterFactory;
import id.or.qodr.dailymuadz.classes.DailyData;
import id.or.qodr.dailymuadz.model.StudentModel;

public class DetailNoteStudent extends AppCompatActivity {

    private RecyclerView rvDaily;
    private DailyData dailyData;
    private List<StudentModel> student;
    private String URL_LIST_NOTE_STUDENT="http://192.168.1.6:1212/api/student/";
    private static final String URL_ADD_NOTE_STUDENT = "http://192.168.1.6:1212/api/notes/add";
    private String URL_END="/notes";
    private String idRequest = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_note_student);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final TextView student_id = (TextView) findViewById(R.id.student_id);
        final TextView users_id = (TextView) findViewById(R.id.users_id);


        Bundle extras = getIntent().getExtras();


        if (extras != null) {
            idRequest = extras.getString("idStudent");
        }

        JsonObjectRequest request = new JsonObjectRequest(URL_LIST_NOTE_STUDENT+idRequest+URL_END, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = new GsonBuilder().registerTypeAdapterFactory(new ArrayAdapterFactory()).create();
                SelfParser result = gson.fromJson(response.toString(), SelfParser.class);

                StudentModel student_info = result.getStudent();
                student_id.setText(student_info.student_name);
                users_id.setText("Kelas : "+student_info.class_at);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        }
        );

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(request);


        dailyData = new DailyData(getApplicationContext());
        student = new ArrayList<StudentModel>();

        rvDaily = (RecyclerView) findViewById(R.id.rcvdetail);

        dailyData.getStudentNote(rvDaily, URL_LIST_NOTE_STUDENT+idRequest+URL_END);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchItem.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_activity) {
            dailyData.addNoteStudent(this, URL_ADD_NOTE_STUDENT, rvDaily, URL_LIST_NOTE_STUDENT, idRequest,"");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

class SelfParser{
    public StudentModel getStudent() {
        return student;
    }

    public StudentModel student;

}
