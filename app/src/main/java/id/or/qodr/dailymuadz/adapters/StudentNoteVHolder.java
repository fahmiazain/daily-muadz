package id.or.qodr.dailymuadz.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import id.or.qodr.dailymuadz.R;

/**
 * Created by adul on 01/12/16.
 */

public class StudentNoteVHolder extends RecyclerView.ViewHolder {

    public TextView student_note, category, note_id;

    public StudentNoteVHolder(View itemView) {
        super(itemView);
        student_note = (TextView) itemView.findViewById(R.id.student_note);
        category = (TextView) itemView.findViewById(R.id.category);
        note_id = (TextView) itemView.findViewById(R.id.note_id);
    }
}
