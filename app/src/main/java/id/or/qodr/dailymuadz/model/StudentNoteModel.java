package id.or.qodr.dailymuadz.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by adul on 30/11/16.
 */

public class StudentNoteModel implements Parcelable {

    public String id,student_id,student_note,category,users_id,create_by,update_by,create_at,update_at;

    protected StudentNoteModel(Parcel in) {
        id = in.readString();
        student_id = in.readString();
        student_note = in.readString();
        category = in.readString();
        users_id = in.readString();
        create_by = in.readString();
        update_by = in.readString();
        create_at = in.readString();
        update_at = in.readString();
    }

    public static final Creator<StudentNoteModel> CREATOR = new Creator<StudentNoteModel>() {
        @Override
        public StudentNoteModel createFromParcel(Parcel in) {
            return new StudentNoteModel(in);
        }

        @Override
        public StudentNoteModel[] newArray(int size) {
            return new StudentNoteModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(student_id);
        parcel.writeString(student_note);
        parcel.writeString(category);
        parcel.writeString(users_id);
        parcel.writeString(create_by);
        parcel.writeString(update_by);
        parcel.writeString(create_at);
        parcel.writeString(update_at);
    }
}
