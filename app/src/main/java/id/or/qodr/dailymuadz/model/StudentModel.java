package id.or.qodr.dailymuadz.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by adul on 28/11/16.
 */

public class StudentModel implements Parcelable {
    public String id,NIS,student_name,class_at,create_by,update_by,create_at,update_at, notes_count;

    protected StudentModel(Parcel in) {
        id = in.readString();
        NIS = in.readString();
        student_name = in.readString();
        class_at = in.readString();
        create_by = in.readString();
        update_by = in.readString();
        create_at = in.readString();
        update_at = in.readString();
        notes_count = in.readString();
    }

    public static final Creator<StudentModel> CREATOR = new Creator<StudentModel>() {
        @Override
        public StudentModel createFromParcel(Parcel in) {
            return new StudentModel(in);
        }

        @Override
        public StudentModel[] newArray(int size) {
            return new StudentModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(NIS);
        dest.writeString(student_name);
        dest.writeString(class_at);
        dest.writeString(create_by);
        dest.writeString(update_by);
        dest.writeString(create_at);
        dest.writeString(update_at);
        dest.writeString(notes_count);
    }
}
