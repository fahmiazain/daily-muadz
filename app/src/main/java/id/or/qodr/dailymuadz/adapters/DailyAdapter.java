package id.or.qodr.dailymuadz.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import id.or.qodr.dailymuadz.R;
import id.or.qodr.dailymuadz.classes.DailyData;
import id.or.qodr.dailymuadz.fragment.ActivityDaily;
import id.or.qodr.dailymuadz.model.DailyModel;
import id.or.qodr.dailymuadz.model.LessionModel;

/**
 * Created by muhammad on 18/11/16.
 */
public class DailyAdapter extends RecyclerView.Adapter<DailyVHolder> {

    private List<DailyModel> dailyItemList;
    private Context context;
    private DailyData dailyData;
    RecyclerView rvDaily;
    private List<DailyModel> daily;
    private String URL_LIST_ACTIVITY="http://192.168.1.6:1212/api/users/1/daily";
    private String URL_ADD_ACTIVITY="http://192.168.1.6:1212/api/daily";

    public DailyAdapter(Context context, List<DailyModel> dailyItemList) {
        this.dailyItemList = dailyItemList;
        this.context = context;
    }

    @Override
    public DailyVHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_daily, null);

        DailyVHolder rcv = new DailyVHolder(layoutView);
        dailyData = new DailyData(context);


        return rcv;
    }

    @Override
    public void onBindViewHolder(final DailyVHolder holder, final int position) {
        final String id_daily = dailyItemList.get(position).id.toString();
        holder.daily_id.setText("id : "+id_daily);
        holder.class_at.setText("class "+dailyItemList.get(position).class_at);
        holder.lession_id.setText("pelajaran "+dailyItemList.get(position).lession_id);
        holder.section_lession.setText(dailyItemList.get(position).section_lession);
        holder.time_lession.setText("Jam "+dailyItemList.get(position).time_lession);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            public boolean wrapInscroll;

            @Override
            public void onClick(View view) {
                final MaterialDialog dialog = new MaterialDialog.Builder(context)
                        .title("Detail Daily")
                        .customView(R.layout.form_detail_daily, wrapInscroll)
                        .cancelable(false)
                        .positiveText(android.R.string.ok)
                        .show();
                TextView lession_name = (TextView) dialog.getCustomView().findViewById(R.id.lession_name);
                TextView timeLession = (TextView) dialog.findViewById(R.id.time_lession);
                TextView class_id = (TextView) dialog.findViewById(R.id.class_at);
                TextView section_less = (TextView) dialog.findViewById(R.id.section_lession);


                lession_name.setText("Pelajaran "+dailyItemList.get(position).lession_id);
                timeLession.setText("Jam ke "+dailyItemList.get(position).time_lession);
                class_id.setText("Kelas "+dailyItemList.get(position).class_at);
                section_less.setText(dailyItemList.get(position).section_lession);
                section_less.setCursorVisible(true);
                section_less.setFocusableInTouchMode(true);
                section_less.setInputType(InputType.TYPE_TEXT_FLAG_IME_MULTI_LINE);
                section_less.requestFocus();
                section_less.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        dailyData.editDaily("http://192.168.1.6:1212/api/daily/"+id_daily,rvDaily, dialog,
                                dailyItemList.get(position).lession_id,
                                dailyItemList.get(position).time_lession,
                                dailyItemList.get(position).class_at);
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });

                dialog.show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return this.dailyItemList.size();
    }
}