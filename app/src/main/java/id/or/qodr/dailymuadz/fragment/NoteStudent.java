package id.or.qodr.dailymuadz.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.or.qodr.dailymuadz.DashboardActivity;
import id.or.qodr.dailymuadz.R;
import id.or.qodr.dailymuadz.adapters.StudentAdapter;
import id.or.qodr.dailymuadz.classes.DailyData;
import id.or.qodr.dailymuadz.model.StudentModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoteStudent extends Fragment {

    RecyclerView rvDaily;
    private DailyData dailyData;
    private List<StudentModel> student;
    private String URL_LIST_ACTIVITY="http://192.168.1.6:1212/api/student";
    private String URL_LIST_SEARCH="http://192.168.1.6:1212/api/student/search";
    private String URL_ADD_ACTIVITY="http://192.168.1.6:1212/api/notes/add";
    private String filterName = "";
    private int filterCategory = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view= inflater.inflate(R.layout.form_list_student, container, false);
        getActivity().setTitle("Catatan Murid");
        setHasOptionsMenu(true);

        student = new ArrayList<StudentModel>();
        dailyData = new DailyData(getActivity());

        rvDaily = (RecyclerView) view.findViewById(R.id.recyclerViewStudent);

        dailyData.getStudent(rvDaily, URL_LIST_ACTIVITY);

        ((DashboardActivity) getActivity()).setActionbarTitle("Catatan Murid");
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_search).setVisible(true);
        menu.findItem(R.id.action_activity).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                filterName = query;
                if (filterCategory == 0) {
                    dailyData.searchStudent(URL_LIST_SEARCH,rvDaily,query);
                    dailyData.getStudent(rvDaily, URL_LIST_ACTIVITY);
                } else {
//                    dailyData.searchStudent(URL_LIST_ACTIVITY,rvDaily,query);
                    Toast.makeText(getContext(), "gagal mencari", Toast.LENGTH_SHORT).show();
                }

                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                filterName = "";
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }
}
