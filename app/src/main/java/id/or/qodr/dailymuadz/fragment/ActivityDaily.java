package id.or.qodr.dailymuadz.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import id.or.qodr.dailymuadz.DashboardActivity;
import id.or.qodr.dailymuadz.R;
import id.or.qodr.dailymuadz.adapters.DailyAdapter;
import id.or.qodr.dailymuadz.classes.DailyData;
import id.or.qodr.dailymuadz.model.DailyModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ActivityDaily extends Fragment {

    ProgressDialog progress;
    RecyclerView rvDaily;
    private DailyData dailyData;
    private List<DailyModel> daily;
    private String URL_LIST_ACTIVITY="http://192.168.1.6:1212/api/users/1/daily";
    private String URL_ADD_ACTIVITY="http://192.168.1.6:1212/api/daily";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.form_activity_daily, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

        dailyData = new DailyData(getActivity());

        daily = new ArrayList<DailyModel>();
        rvDaily = (RecyclerView) getActivity().findViewById(R.id.recyclerViewDaily);


        dailyData.getDaily(rvDaily, URL_LIST_ACTIVITY);

        ((DashboardActivity) getActivity()).setActionbarTitle("Aktifitas Harian");


//        Fragment currentFragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
//        FragmentTransaction ft = getFragmentManager().beginTransaction();
//        ft.detach(currentFragment);
//        ft.attach(currentFragment);
//        ft.commit();
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.action_activity).setVisible(false);
        inflater.inflate(R.menu.main2, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_activity_2) {
            dailyData.addDailyActivity(getActivity(), URL_ADD_ACTIVITY, rvDaily, URL_LIST_ACTIVITY);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
