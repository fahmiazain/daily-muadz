package id.or.qodr.dailymuadz;

import android.app.ProgressDialog;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.Stack;

import id.or.qodr.dailymuadz.classes.DailyData;
import id.or.qodr.dailymuadz.fragment.ActivityDaily;
import id.or.qodr.dailymuadz.fragment.Dashboard;
import id.or.qodr.dailymuadz.fragment.NoteStudent;

public class DashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    ProgressDialog progress;
    private DailyData dailyData;
    private RecyclerView rvDaily;
    private String URL_LIST_ACTIVITY="http://192.168.1.8:1212/users/1/daily";
    private String URL_LIST_STUDENT="http://192.168.1.8:1212/api/student";
    private String URL_ADD_ACTIVITY="http://192.168.1.8:1212/api/daily";
    private FragmentManager fragmentActivity;
    private Stack<Fragment> backFragment;
    private String filterName = "";
    private int filterCategory = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        backFragment = new Stack<>();
        fragmentActivity = getSupportFragmentManager();
        replaceFragment(null, new Dashboard());
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        dailyData = new DailyData(this);

        setActionbarTitle("Daily Muadz");
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,drawer,toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(this);

//        View emptyView = findViewById(R.id.empty);
//        if (daily.isEmpty()) {
//            rvDaily.setVisibility(View.GONE);
//            emptyView.setVisibility(View.VISIBLE);
//        }
//        else {
//            rvDaily.setVisibility(View.VISIBLE);
//            emptyView.setVisibility(View.GONE);
//        }

//        daily = new ArrayList<DailyModel>();
//
//        rvDaily = (RecyclerView) findViewById(R.id.recyclerViewDaily);
//
//        dailyData.getDaily(rvDaily, URL_LIST_ACTIVITY);

    }

    public void setActionbarTitle(String title){
        TextView textTitle = (TextView) findViewById(R.id.action_bar_title);
        textTitle.setText(title);
    }

    private void replaceFragment(Fragment fromFragment, Fragment toFragment) {
        if (fromFragment != null) {
            backFragment.push(fromFragment);
            fragmentActivity.beginTransaction()
                    .replace(R.id.container, fromFragment)
                    .commit();
        }else {
            fragmentActivity.beginTransaction()
                    .replace(R.id.container,toFragment)
                    .commit();
        }
    }


    @Override
    public void onBackPressed() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.container);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

        }else if(currentFragment instanceof Dashboard){
            super.onBackPressed();
        }else if (currentFragment instanceof ActivityDaily) {
            replaceFragment(null, new Dashboard());
        }else if (currentFragment instanceof NoteStudent) {
            replaceFragment(null, new Dashboard());
        }
//        DrawerLayout drawerLayout = null;
//        if (currentFragment instanceof Dashboard) {
//            drawerLayout = (DrawerLayout) currentFragment.getView().findViewById(R.id.drawer_layout);
//            searchView = ((Dashboard) currentFragment).getSearchView();
//        }
//
//        if (currentFragment instanceof Dashboard && drawerLayout.isDrawerOpen(GravityCompat.START)) {
//            drawerLayout.closeDrawer(GravityCompat.START);
//        } else if (currentFragment instanceof Dashboard && !searchView.isIconified()) {
//            searchView.setIconified(true);
//        } else if (backFragment.size() > 0) {
//            replaceFragment(null, backFragment.pop());
//        } else {
//            super.onBackPressed();
//        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchItem.setVisible(false);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
//                filterName = query;
//                    if (filterCategory == 0) {
//                        dailyData.getStudent(rvDaily, URL_LIST_STUDENT);
//                    } else {
//                        dailyData.getStudent(rvDaily, URL_LIST_STUDENT);
//                    }

                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                filterName = "";
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_activity) {
            dailyData.addDailyActivity(this, URL_ADD_ACTIVITY, rvDaily, URL_LIST_ACTIVITY);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        switch (id) {
            case R.id.dashboard:
                fragment = new Dashboard();
                break;
            case R.id.activity_harian:
                progress = new ProgressDialog(this);
                progress.setMessage("Mohon Tunggu...");
                progress.show();
                Runnable progressRunnable = new Runnable() {

                    @Override
                    public void run() {
                        progress.cancel();
                    }
                };

                Handler pdCanceller = new Handler();
                pdCanceller.postDelayed(progressRunnable, 700);
                fragment = new ActivityDaily();

                break;
            case R.id.note_murid:
                progress = new ProgressDialog(this);
                progress.setMessage("Mohon Tunggu...");
                progress.show();
                progressRunnable = new Runnable() {

                    @Override
                    public void run() {
                        progress.cancel();
                    }
                };

                pdCanceller = new Handler();
                pdCanceller.postDelayed(progressRunnable, 700);
                fragment = new NoteStudent();
                break;
            case R.id.setting:
                break;
            case R.id.logout:
                break;
        }
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.container, fragment);
            ft.commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
