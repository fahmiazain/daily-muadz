package id.or.qodr.dailymuadz.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import id.or.qodr.dailymuadz.AboutActivity;
import id.or.qodr.dailymuadz.DashboardActivity;
import id.or.qodr.dailymuadz.LoginActivity;
import id.or.qodr.dailymuadz.R;
import id.or.qodr.dailymuadz.classes.DailyData;

/**
 * A simple {@link Fragment} subclass.
 */
public class Dashboard extends Fragment  {

    private AppCompatActivity activity;
    private RelativeLayout btnDaily,btnStudent,btnAbout,btnLogout;
    private DailyData dailyData;
    private RecyclerView rvDaily;
    private String URL_LIST_ACTIVITY="http://192.168.1.6:1212/api/users/1/daily";
    private String URL_ADD_ACTIVITY="http://192.168.1.6:1212/api/daily";

    private View mainView;
    private ProgressDialog progress;

    private SearchView searchView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainView =  inflater.inflate(R.layout.view_dashboard, container, false);
        getActivity().setTitle("Daily Muadz");
        setHasOptionsMenu(true);

        dailyData = new DailyData(getActivity());

        btnDaily = (RelativeLayout) mainView.findViewById(R.id.btn_daily);
        btnStudent = (RelativeLayout) mainView.findViewById(R.id.btn_student);
        btnAbout = (RelativeLayout) mainView.findViewById(R.id.btn_about);
        btnLogout = (RelativeLayout) mainView.findViewById(R.id.btn_logout);

        btnAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AboutActivity.class);
                getActivity().startActivity(intent);
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
            }
        });


        btnDaily.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress = new ProgressDialog(getActivity());
                progress.setMessage("Mohon Tunggu...");
                progress.show();
                Runnable progressRunnable = new Runnable() {

                    @Override
                    public void run() {
                        progress.cancel();
                    }
                };

                Handler pdCanceller = new Handler();
                pdCanceller.postDelayed(progressRunnable, 700);
                Fragment fragmentDaily = new ActivityDaily();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.container, fragmentDaily);
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        btnStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress = new ProgressDialog(getActivity());
                progress.setMessage("Mohon Tunggu...");
                progress.show();
                Runnable progressRunnable = new Runnable() {

                    @Override
                    public void run() {
                        progress.cancel();
                    }
                };

                Handler pdCanceller = new Handler();
                pdCanceller.postDelayed(progressRunnable, 700);
                Fragment fragmentStudent = new NoteStudent();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.container, fragmentStudent);
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        ((DashboardActivity) getActivity()).setActionbarTitle("Dashboard");
//        DrawerLayout drawer = (DrawerLayout) mainView.findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                getActivity(),drawer,toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.addDrawerListener(toggle);
//        toggle.syncState();
//
//        NavigationView navigationView = (NavigationView) mainView.findViewById(R.id.navigationView);
//        navigationView.setNavigationItemSelectedListener(this);
        return mainView;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_search).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//        Fragment fragment = null;
//        switch (id) {
//            case R.id.dashboard:
//                fragment = new Dashboard();
//                break;
//            case R.id.activity_harian:
//                fragment = new ActivityDaily();
//                break;
//            case R.id.note_murid:
//                fragment = new NoteStudent();
//                break;
//            case R.id.setting:
//                break;
//            case R.id.logout:
//                break;
//        }
//        if (fragment != null) {
//            FragmentTransaction ft = getFragmentManager().beginTransaction();
//            ft.replace(R.id.container, fragment);
//            ft.addToBackStack(null);
//            ft.commit();
//        }
//        DrawerLayout drawer = (DrawerLayout) getView().findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//        return true;
//    }

    public SearchView getSearchView() {
        return searchView = (SearchView) getView().findViewById(R.id.action_search);
    }
}
