package id.or.qodr.dailymuadz.adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.or.qodr.dailymuadz.R;
import id.or.qodr.dailymuadz.classes.DailyData;
import id.or.qodr.dailymuadz.model.StudentModel;
import id.or.qodr.dailymuadz.model.StudentNoteModel;
import id.or.qodr.dailymuadz.model.UsersModel;

import static android.content.Intent.getIntent;

/**
 * Created by adul on 01/12/16.
 */

public class StudentNoteAdapter extends RecyclerView.Adapter<StudentNoteVHolder> {

    private static final String URL_LIST_USER_STUDENT = "http://192.168.1.6:1212/api/student/";
    private final String END_URL="/notes";
    private List<StudentNoteModel> noteItemList;
    private List<StudentModel> studentList;
    private DailyData dailyData;
    private Context context;

    public StudentNoteAdapter( Context context,List<StudentNoteModel> noteItemList) {
        this.context = context;
        this.noteItemList = noteItemList;
    }



    @Override
    public StudentNoteVHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_note, null);
        StudentNoteVHolder rcv = new StudentNoteVHolder(layoutView);
        dailyData = new DailyData(context);


        return rcv;
    }

    @Override
    public void onBindViewHolder(final StudentNoteVHolder holder, final int position) {
        final String id_note = String.valueOf(noteItemList.get(position).id.trim().toString());
        holder.note_id.setText("id : "+id_note);
        holder.student_note.setText(noteItemList.get(position).student_note);
//        final String isNull=noteItemList.get(position).category==null?"Uncategory":noteItemList.get(position).category;
        holder.category.setText(noteItemList.get(position).category);
        String category=noteItemList.get(position).category.trim();
        if (category.toLowerCase().equals("prestasi")) {
            holder.category.setTextColor(Color.GREEN);
        }else if (category.toLowerCase().equals("peringatan")){
            holder.category.setTextColor(Color.parseColor("#FDD835"));
        }else if (category.toLowerCase().equals("hukuman")){
            holder.category.setTextColor(Color.RED);
        }else if (category.toLowerCase().equals("apresiasi")){
            holder.category.setTextColor(Color.BLUE);
        }else if (category.toLowerCase().equals("umum")){
            holder.category.setTextColor(Color.BLACK);
        }



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final MaterialDialog dialog = new MaterialDialog.Builder(view.getContext())
                        .title("Detail Catatan")
                        .customView(R.layout.form_detail_note, true)
                        .positiveText(android.R.string.ok)
                        .cancelable(false)
                        .build();
                dialog.show();

                TextView student_note = (TextView) dialog.getCustomView().findViewById(R.id.student_note);
                final TextView category = (TextView) dialog.getCustomView().findViewById(R.id.category);
                final TextView username = (TextView) dialog.getCustomView().findViewById(R.id.username);

                    JsonObjectRequest request = new JsonObjectRequest(URL_LIST_USER_STUDENT + noteItemList.get(position).student_id + END_URL, null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Gson gson = new GsonBuilder().registerTypeAdapterFactory(new ArrayAdapterFactory()).create();
                            NoteDetailResult result = gson.fromJson(response.toString(), NoteDetailResult.class);

                            UsersModel users_info = result.getStudent().getStudent_note().get(position).getUsers();
                            username.setText("Catatan oleh : "+users_info.username);


                        }

                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    );

                    RequestQueue queue = Volley.newRequestQueue(context);
                    queue.add(request);

                final String categorydetail=noteItemList.get(position).category.trim();

                if (categorydetail.equals("Prestasi")) {
                    category.setTextColor(Color.GREEN);
                }else if (categorydetail.toLowerCase().equals("peringatan")){
                    category.setTextColor(Color.parseColor("#FDD835"));
                }else if (categorydetail.toLowerCase().equals("hukuman")){
                    category.setTextColor(Color.RED);
                }else if (categorydetail.toLowerCase().equals("apresiasi")){
                    category.setTextColor(Color.BLUE);
                }else if (categorydetail.toLowerCase().equals("umum")){
                    category.setTextColor(Color.BLACK);
                }else{
                    category.setTextColor(Color.WHITE);
                }
                category.setText(noteItemList.get(position).category);

                student_note.setText(noteItemList.get(position).student_note);
                student_note.setCursorVisible(true);
                student_note.setFocusableInTouchMode(true);
                student_note.setInputType(InputType.TYPE_TEXT_FLAG_IME_MULTI_LINE);
                student_note.requestFocus();
                student_note.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        dailyData.editNoteStudent("http://192.168.1.6:1212/api/notes/"+id_note, dialog, categorydetail);
                        Toast.makeText(context, "ontextchange"+id_note, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });


            }
        });
    }

    @Override
    public int getItemCount() {
        return this.noteItemList.size();
    }
}

class NoteDetailResult{
    public SelfParser student;

    public SelfParser getStudent() {
        return student;
    }
}

class SelfParser {
    public ArrayList<SanParser> student_note;

    public ArrayList<SanParser> getStudent_note() {
        return student_note;
    }
}

class SanParser{
    public UsersModel users;

    public UsersModel getUsers() {
        return users;
    }
}