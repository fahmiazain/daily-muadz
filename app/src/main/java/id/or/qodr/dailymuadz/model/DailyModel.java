package id.or.qodr.dailymuadz.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by muhammad on 18/11/16.
 */
public class DailyModel implements Parcelable {

    public String id, users_id, class_at, lession_id, section_lession,
            time_lession, created_by, updated_by, created_at, updated_at;

    protected DailyModel(Parcel in) {
        id = in.readString();
        users_id = in.readString();
        class_at = in.readString();
        lession_id = in.readString();
        section_lession = in.readString();
        time_lession = in.readString();
        created_by = in.readString();
        updated_by = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
    }

    public static final Creator<DailyModel> CREATOR = new Creator<DailyModel>() {
        @Override
        public DailyModel createFromParcel(Parcel in) {
            return new DailyModel(in);
        }

        @Override
        public DailyModel[] newArray(int size) {
            return new DailyModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(users_id);
        dest.writeString(class_at);
        dest.writeString(lession_id);
        dest.writeString(section_lession);
        dest.writeString(time_lession);
        dest.writeString(created_by);
        dest.writeString(updated_by);
        dest.writeString(created_at);
        dest.writeString(updated_at);
    }
}
